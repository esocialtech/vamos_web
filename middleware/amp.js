export default function ({ route, redirect }) {
  if (route.path.endsWith('/amp')) {
    const { path, query, hash } = route
    const nextPath = '/amp' + path.replace(/\/amp$/, '')
    const nextRoute = { path: nextPath, query, hash }

    redirect(nextRoute)
  }
}
