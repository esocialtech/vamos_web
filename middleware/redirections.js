import { redirections } from '../constants/redirections'

export default function ({ route, redirect }) {
  redirections.forEach((redirection) => {
    if (redirection.from === route.path) {
      console.log('path')
      console.log(route.path)

      redirect(redirection.to)
    }
  })
}
