export const documentTypes = [
  {
    value: 'document_number',
    text: 'Fotocopia DNI'
  }, {
    value: 'company_document_number',
    text: 'CIF'
  }, {
    value: 'last_income_tax',
    text: 'Última renta'
  }, {
    value: 'work_life_report',
    text: 'Informe vida laboral'
  }, {
    value: 'bank_receipt',
    text: 'Recibo bancario'
  }, {
    value: 'last_paysheet_document',
    text: 'Última nómina'
  }, {
    value: 'receipt',
    text: 'Copia de factura'
  }, {
    value: 'freelance_registration',
    text: 'Alta censal autónomo'
  }, {
    value: 'model_390',
    text: 'Resumen IVA año anterior.(390)'
  }, {
    value: 'model_303',
    text: 'Trimestre IVA año en curso.(303)'
  }, {
    value: 'model_200',
    text: 'Último impuesto de sociedades'
  }, {
    value: 'assignee_document',
    text: 'DNI apoderado'
  }, {
    value: 'loss_balance',
    text: 'Balance de pérdidas y ganancias'
  }, {
    value: 'polity',
    text: 'Escritura de constitución'
  }, {
    value: 'power_of_attorney',
    text: 'Escritura de poder'
  }
]

export function getDocumentTypes (value) {
  return documentTypes.find(documentTypes => documentTypes.value === value)
}
