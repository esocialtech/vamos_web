export const documentTypes = [
  {
    value: 'front_dni',
    text: 'DNI Frontal'
  }, {
    value: 'back_dni',
    text: 'DNI Trasero'
  }, {
    value: 'bank_receipt',
    text: 'Recibo bancario'
  }, {
    value: 'last_paysheet_document',
    text: 'Última nómina'
  }
]

export function getDocumentTypes (value) {
  return documentTypes.find(documentTypes => documentTypes.value === value)
}
