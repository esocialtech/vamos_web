const axios = require('axios')

module.exports = async function getRoutes () {
  const instance = axios.create({
    baseURL: process.env.BASE_URL
  })

  const routes = []
  await loadCarBrands(routes, instance)
  await loadCarModels(routes, instance)
  await loadCars(routes, instance)
  await loadBlogArticles(routes, instance)
  await loadCarTypeFacet(routes, instance)
  await loadCarFuelTypeFacet(routes, instance)
  await loadDeadlineFacet(routes, instance)

  return routes
}

async function loadCarBrands (routes, instance) {
  let brands = []

  try {
    const response = await instance.get('car-brands')
    brands = response.data['hydra:member']
  } catch (e) {
  }

  for (let i = 0; i < brands.length; i += 1) {
    const brand = brands[i]
    routes.push({
      url: `renting-${brand.slug}`,
      priority: brand.priority ? brand.priority : 1
    })
  }
}

async function loadCarModels (routes, instance) {
  let models = []

  try {
    const response = await instance.get('car-models')
    models = response.data['hydra:member']
  } catch (e) {
  }

  for (let i = 0; i < models.length; i += 1) {
    const model = models[i]
    routes.push({
      url: `renting-${model.brand.slug}/${model.slug}`,
      priority: model.priority ? model.priority : 1
    })
  }
}

async function loadCars (routes, instance) {
  let cars = []

  try {
    const response = await instance.get('cars')
    cars = response.data['hydra:member']
  } catch (e) {
  }

  for (let i = 0; i < cars.length; i += 1) {
    const car = cars[i]
    routes.push({
      url: `renting-${car.model.brand.slug}/${car.model.slug}/${car.slug}`,
      priority: car.priority ? car.priority : 1
    })
  }
}

async function loadBlogArticles (routes, instance) {
  let articles = []

  try {
    const response = await instance.get('articles')
    articles = response.data['hydra:member']
  } catch (e) {
  }

  for (let i = 0; i < articles.length; i += 1) {
    const article = articles[i]
    routes.push({
      url: `noticias-coches/${article.slug}`,
      priority: article.priority ? article.priority : 1
    })
  }
}

async function loadCarTypeFacet (routes, instance) {
  let carTypes = []

  try {
    const response = await instance.get('car-types', { params: { enabled: true } })
    carTypes = response.data['hydra:member']
  } catch (e) {
  }

  for (let i = 0; i < carTypes.length; i += 1) {
    const carType = carTypes[i]
    routes.push({
      url: `todos-los-coches/${carType.slug}`,
      priority: carType.priority ? carType.priority : 1
    })

    routes.push({
      url: `renting-coches-particulares/${carType.slug}`,
      priority: carType.priority ? carType.priority : 1
    })

    routes.push({
      url: `renting-coches-autonomos/${carType.slug}`,
      priority: carType.priority ? carType.priority : 1
    })

    routes.push({
      url: `renting-coches-empresas/${carType.slug}`,
      priority: carType.priority ? carType.priority : 1
    })
  }
}

async function loadCarFuelTypeFacet (routes, instance) {
  let carFuelTypes = []

  try {
    const response = await instance.get('car-fuel-types', { params: { enabled: true } })
    carFuelTypes = response.data['hydra:member']
  } catch (e) {
  }

  for (let i = 0; i < carFuelTypes.length; i += 1) {
    const carFuelType = carFuelTypes[i]
    routes.push({
      url: `todos-los-coches/${carFuelType.slug}`,
      priority: carFuelType.priority ? carFuelType.priority : 1
    })

    routes.push({
      url: `renting-coches-particulares/${carFuelType.slug}`,
      priority: carFuelType.priority ? carFuelType.priority : 1
    })

    routes.push({
      url: `renting-coches-autonomos/${carFuelType.slug}`,
      priority: carFuelType.priority ? carFuelType.priority : 1
    })

    routes.push({
      url: `renting-coches-empresas/${carFuelType.slug}`,
      priority: carFuelType.priority ? carFuelType.priority : 1
    })
  }
}

async function loadDeadlineFacet (routes, instance) {
  let deadlines = []

  try {
    const response = await instance.get('deadlines', { params: { enabled: true } })
    deadlines = response.data['hydra:member']
  } catch (e) {
  }

  for (let i = 0; i < deadlines.length; i += 1) {
    const deadline = deadlines[i]
    routes.push({
      url: `todos-los-coches/${deadline.slug}`,
      priority: deadline.priority ? deadline.priority : 1
    })

    routes.push({
      url: `renting-coches-particulares/${deadline.slug}`,
      priority: deadline.priority ? deadline.priority : 1
    })

    routes.push({
      url: `renting-coches-autonomos/${deadline.slug}`,
      priority: deadline.priority ? deadline.priority : 1
    })

    routes.push({
      url: `renting-coches-empresas/${deadline.slug}`,
      priority: deadline.priority ? deadline.priority : 1
    })
  }
}
