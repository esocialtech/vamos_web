export default {
  methods: {
    handleErrors (field) {
      const errors = []
      if (!field.$dirty) {
        return errors
      }

      if ('required' in field) {
        !field.required && errors.push('* Campo requerido')
      }

      if ('email' in field) {
        !field.email && errors.push('Correo electrónico inválido')
      }

      if ('minLength' in field) {
        !field.minLength && errors.push(`El campo debe contener por lo menos ${field.$params.minLength.min} caracteres`)
      }

      if ('maxLength' in field) {
        !field.maxLength && errors.push(`El campo debe contener como máximo ${field.$params.maxLength.max} caracteres`)
      }

      if ('valid' in field) {
        !field.valid && errors.push('Campo inválido')
      }

      return errors
    }
  }
}
