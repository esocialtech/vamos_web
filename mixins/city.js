export default {
  data: () => ({
    cities: []
  }),

  methods: {
    async loadCity () {
      try {
        const { data } = await this.$axios.get('cities', {
          params: {
            postalCode: this.form.address.postalCode
          }
        })
        this.cities = data['hydra:member']
        if (this.cities.length > 0) {
          if (this.form.address.city === '') {
            this.form.address.city = this.cities[0]
          }
          this.form.address.province = this.cities[0].province
        }
      } catch (e) {
        this.$toast.error(e)
      } finally {
        this.loading = false
      }
    }
  }
}
