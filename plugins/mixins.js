import Vue from 'vue'
import vuelidateMixin from '~/mixins/vuelidate'

Vue.mixin(vuelidateMixin)
