import colors from 'vuetify/es5/util/colors'
import es from 'vuetify/es5/locale/es'

// Dotenv
// https://code.luasoftware.com/tutorials/vuejs/nuxt-dotenv-configuraton-for-development-and-production/

const modifyHtml = (html) => {
  html = html.replace('<noscript data-n-head="ssr" data-hid="local-font-noscript"></noscript>', '')
  html = html.replace('<noscript data-n-head="ssr" data-hid="custom-font-noscript"></noscript>', '')
  html = html.replace('<noscript data-n-head="ssr" data-hid="google-font-noscript"><link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Space+Grotesk:wght@300;400;500;600;700&display=swap"></noscript>', '')

  return html
}

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s | Vamos',
    title: '▶  Renting de Coches | Sin Entrada | Todo incluido',
    htmlAttrs: {
      lang: 'es'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Descubre el mejor servicio de renting de coches para empresas, autónomos y particulares con todo incluido ✅ Sin entrada ni fianza y con cuota fija ✅  ¿Vamos?' },
      { hid: 'robots', name: 'robots', content: 'index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'google-site-verification', content: 'wa0o7EjFbcnyewZRfvhUSr598CtKLTs6s2KhoKAtKLk' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        src: '//js.hs-scripts.com/20086312.js',
        body: true,
        async: true,
        defer: true,
        id: 'hs-script-loader'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/mixins',
    '~/plugins/vuelidate',
    '~/plugins/gtm',
    { src: '~/plugins/vue-countdown', ssr: false },
    { src: '~/plugins/cookie', ssr: false }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    '~/components/',
    { path: '~/components/layout/' },
    { path: '~/components/modules/' }
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@aceforth/nuxt-optimized-images',
    '@nuxtjs/eslint-module',
    '@nuxtjs/moment',
    '@nuxtjs/vuetify',
    '@nuxt/postcss8',
    'nuxt-font-loader',
    '@nuxtjs/google-analytics'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    'nuxt-compress',
    '@nuxtjs/toast',
    '@nuxtjs/gtm',
    '@nuxtjs/sentry',
    ['nuxt-trustbox-module', {
      businessunitId: '5d49a3e5920cc70001783f25',
      businessunitName: 'https://trustpilot.com/review/vamos.es',
      locale: 'es-es'
    }],
    '@nuxtjs/amp'
  ],

  axios: {},

  amp: {
    origin: process.env.ORIGIN_URL || 'https://www.vamos.es',
    mode: false,
    css: '~/assets/default.amp.css'
  },

  gtm: {
    pageTracking: true,
    pageViewEventName: 'nuxtRoute'
  },

  googleAnalytics: {
    id: 'UA-140604623-2'
  },

  fontLoader: {
    url: {
      local: '/fonts/font-face.css',
      custom: 'https://cdn.jsdelivr.net/npm/@mdi/font@latest/css/materialdesignicons.min.css'
    },
    prefetch: true,
    preconnect: true,
    stylesheet: true
  },

  moment: {
    defaultLocale: 'es',
    locales: ['es']
  },

  optimizedImages: {
    optimizeImages: true
  },

  toast: {
    position: 'bottom-right',
    duration: 5000,
    iconPack: 'mdi',
    theme: 'outline'
  },

  vuetify: {
    lang: {
      locales: { es },
      current: 'es'
    },
    defaultAssets: false,
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    theme: {
      themes: {
        light: {
          primary: '#000000',
          accent: colors.grey.darken3,
          secondary: '#FF9300',
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      },
      options: {
        customProperties: true
      }
    },
    breakpoint: {
      thresholds: {
        lg: 1400
      }
    }
  },

  publicRuntimeConfig: {
    axios: {
      baseURL: process.env.BASE_URL || 'https://web-api.vamos.training'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  },

  server: {
    port: process.env.NODE_ENV === 'development' ? 4300 : null
  },

  sentry: {
    dsn: 'https://c45912b5e9e54ce6b248ffa1f768c16d@o1072938.ingest.sentry.io/6072443',
    disabled: process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'prod',
    tracing: {
      tracesSampleRate: 0.2,
      vueOptions: {
        tracing: true,
        tracingOptions: {
          hooks: ['mount', 'update'],
          timeout: 2000,
          trackComponents: true
        }
      },
      browserOptions: {}
    },
    // Additional Module Options go here
    // https://sentry.nuxtjs.org/sentry/options
    config: {
      debug: process.env.NODE_ENV === 'development',
      environment: process.env.NODE_ENV
      // Add native Sentry config here
      // https://docs.sentry.io/platforms/javascript/guides/vue/configuration/options/
    }
  },

  router: {
    trailingSlash: false,
    middleware: [
      'amp',
      'redirections',
      'slash'
    ]
  },

  hooks: {
    'generate:page': (page) => {
      page.html = modifyHtml(page.html)
    },
    'render:route': (url, page, {
      req,
      res
    }) => {
      if (url.startsWith('/amp/')) {
        page.html = modifyHtml(page.html)
      }
    }
  }
}
