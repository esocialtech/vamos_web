export const state = () => ({
  selectedCar: null,
  firstForm: null,
  secondForm: null
})

export const getters = {
  selectedCar: state => state.selectedCar,
  firstForm: state => state.firstForm,
  secondForm: state => state.secondForm
}

export const mutations = {
  setSelectedCar (state, car) {
    state.selectedCar = car
  },
  setFirstForm (state, firstForm) {
    state.firstForm = firstForm
  },
  setSecondForm (state, secondForm) {
    state.secondForm = secondForm
  }
}

export const actions = {
  setSelectedCar ({ commit }, car) {
    commit('setSelectedCar', car)
  },
  setFirstForm ({ commit }, firstForm) {
    commit('setFirstForm', firstForm)
  },
  setSecondForm ({ commit }, secondForm) {
    commit('setSecondForm', secondForm)
  }
}
