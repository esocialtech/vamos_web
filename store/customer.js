export const state = () => ({
  formContact: null,
  searched: false
})

export const getters = {
  formContact: state => state.formContact,
  searched: state => state.searched
}

export const mutations = {
  setFormContact (state, formContact) {
    state.formContact = formContact
  },
  setSearched (state, searched) {
    state.searched = searched
  }
}

export const actions = {
  setFormContact ({ commit }, formContact) {
    commit('setFormContact', formContact)
  },
  setSearched ({ commit }, searched) {
    commit('setSearched', searched)
  }
}
