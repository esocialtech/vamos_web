export const state = () => ({
  appBarMenu: false
})

export const getters = {
  appBarMenu: state => state.appBarMenu
}

export const mutations = {
  setAppBarMenu (state, boolean) {
    state.appBarMenu = boolean
  }
}

export const actions = {
  setAppBarMenu ({ commit }, boolean) {
    commit('setAppBarMenu', boolean)
  }
}
