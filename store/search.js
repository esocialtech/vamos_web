export const state = () => ({
  carTypes: [],
  fuelTypes: [],
  periodTypes: [],
  facet: null,
  cars: null,
  priceFilter: null,
  brandFilter: null,
  transmissionFilter: null,
  colorFilter: null,
  accessoriesFilter: null
})

export const getters = {
  carTypes: state => state.carTypes,
  fuelTypes: state => state.fuelTypes,
  periodTypes: state => state.periodTypes,
  facet: state => state.facet,
  cars: state => state.cars,
  priceFilter: state => state.priceFilter,
  brandFilter: state => state.brandFilter,
  transmissionFilter: state => state.transmissionFilter,
  colorFilter: state => state.colorFilter,
  accessoriesFilter: state => state.accessoriesFilter,
  brands (state, getters) {
    const brands = []

    getters.filteredCars.forEach((car) => {
      if (!brands.includes(car.model.brand)) {
        brands.push(car.model.brand)
      }
    })

    brands.sort((a, b) => (a.name > b.name) ? 1 : ((b.name > a.name) ? -1 : 0))

    return brands
  },
  transmissions (state, getters) {
    const transmissions = []

    getters.filteredCars.forEach((car) => {
      if (!transmissions.includes(car.transmission)) {
        transmissions.push(car.transmission === 'auto' ? { value: 'auto', text: 'Automático' } : { value: 'manual', text: 'Manual' })
      }
    })

    return transmissions
  },
  colors (state, getters) {
    const colors = []

    getters.filteredCars.forEach((car) => {
      car.paymentTypes.forEach((paymentType) => {
        if (paymentType.color.parent && paymentType.published === 1 && !colors.some(item => item['@id'] === paymentType.color.parent['@id'])) {
          colors.push(paymentType.color.parent)
        }
      })
    })

    return colors
  },
  filteredCars (state) {
    if (state.cars === null) {
      return []
    }

    return state.cars.filter((car) => {
      if (state.priceFilter !== null) {
        let hasPrice = false

        if (state.priceFilter.value[0] <= car.defaultPaymentType.totalMonthlyFee && car.defaultPaymentType.totalMonthlyFee < state.priceFilter.value[1]) {
          hasPrice = true
        }

        if (!hasPrice) {
          return false
        }
      }

      if (state.brandFilter !== null && car.model.brand.name !== state.brandFilter.name) {
        return false
      }

      if (state.transmissionFilter !== null && car.transmission !== state.transmissionFilter.value) {
        return false
      }

      if (state.colorFilter !== null) {
        let hasColor = false

        car.paymentTypes.forEach((paymentType) => {
          if (paymentType.published === 1 && paymentType.color.parent && paymentType.color.parent['@id'] === state.colorFilter['@id']) {
            hasColor = true
          }
        })

        if (!hasColor) {
          return false
        }
      }

      return true
    })
  }
}

export const mutations = {
  setCarTypes (state, types) {
    state.carTypes = types
  },

  setFuelTypes (state, types) {
    state.fuelTypes = types
  },

  setPeriodTypes (state, types) {
    state.periodTypes = types
  },

  setFacet (state, facet) {
    state.facet = facet
  },

  setCars (state, cars) {
    state.cars = cars
  },

  setPriceFilter (state, filter) {
    state.priceFilter = filter
  },

  setBrandFilter (state, filter) {
    state.brandFilter = filter
  },

  setTransmissionFilter (state, filter) {
    state.transmissionFilter = filter
  },

  setColorFilter (state, filter) {
    state.colorFilter = filter
  },

  setAccessoriesFilter (state, filter) {
    state.accessoriesFilter = filter
  }
}

export const actions = {
  setCarTypes ({ commit }, types) {
    commit('setCarTypes', types)
  },

  setFuelTypes ({ commit }, types) {
    commit('setFuelTypes', types)
  },

  setPeriodTypes ({ commit }, types) {
    commit('setPeriodTypes', types)
  },

  setFacet ({ commit }, facet) {
    commit('setFacet', facet)
  },

  setCars ({ commit }, cars) {
    commit('setCars', cars)
  },

  setPriceFilter ({ commit }, filter) {
    commit('setPriceFilter', filter)
  },

  setBrandFilter ({ commit }, filter) {
    commit('setBrandFilter', filter)
  },

  setTransmissionFilter ({ commit }, filter) {
    commit('setTransmissionFilter', filter)
  },

  setColorFilter ({ commit }, filter) {
    commit('setColorFilter', filter)
  },

  setAccessoriesFilter ({ commit }, filter) {
    commit('setAccessoriesFilter', filter)
  }
}
